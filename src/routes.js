import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import InitialPage from "./pages/InitialPage";
import Blog from "./pages/Blog/InitialPage";
import NoMatch from "./pages/NoMatch";

const Routes = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={InitialPage} />
      <Route path="/blog" component={Blog} />
      <Route component={NoMatch} />
    </Switch>
  </BrowserRouter>
);

export default Routes;
