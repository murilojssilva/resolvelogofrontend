import React from "react";
import "./style.css";

export default function WhoWeAre() {
  return (
    <div id="whoweare" className="whoweare">
      <h2>Quem somos</h2>
      <p>
        Lorem, ipsum dolor sit amet consectetur adipisicing elit. Obcaecati
        impedit, delectus quos magni quae quis? Ea soluta et numquam! Laudantium
        dolores delectus, iure ex ducimus ullam quo repellat dolorem nesciunt.
      </p>
    </div>
  );
}
