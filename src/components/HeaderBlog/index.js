import React from "react";
import { Link } from "react-router-dom";
import logo from "../../assets/logo.jpg";

export default function HeaderBlog() {
  return (
    <>
      <header class="menu-bg">
        <div class="menu">
          <div class="menu-logo">
            <Link to="/">
              <img src={logo} alt="Resolve Logo" className="logo" />
            </Link>
          </div>
          <nav class="menu-nav">
            <Link to="/blog">Blog</Link>
          </nav>
        </div>
      </header>
    </>
  );
}
