import React from "react";
import "./style.css";
import { Link } from "react-router-dom";
import logo from "../../assets/logo.jpg";

export default function Header() {
  return (
    <>
      <header>
        <div class="menu">
          <div class="menu-logo">
            <Link to="/">
              <img src={logo} alt="Resolve Logo" className="logo" />
            </Link>
          </div>
          <nav class="menu-nav">
            <ul>
              <li>
                <a href="#about">Sobre</a>
              </li>
              <li>
                <a href="#whoweare">Quem somos</a>
              </li>
              <li>
                <a href="#cases">Nossos serviços</a>
              </li>
              <li>
                <a href="#contact">Contato</a>
              </li>
              <li>
                <a href="/blog">Blog</a>
              </li>
            </ul>
          </nav>
        </div>
      </header>
    </>
  );
}
