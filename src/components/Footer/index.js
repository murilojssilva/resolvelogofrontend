import React from "react";
import "./style.css";

export default function Footer() {
  return (
    <div className="footer">
      <div className="footer-info">
        <p>Copyright © 2020 - Resolve Logo, todos os direitos reservados. </p>
        <p>
          Desenvolvidor por{" "}
          <a href="https://linkedin.com/in/murilojssilva">Murilo de Jesus</a>
        </p>
      </div>
    </div>
  );
}
